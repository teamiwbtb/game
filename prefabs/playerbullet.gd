extends Area2D

var speed = 1500

func _physics_process(delta):
	position.x += speed*delta
	
	for i in get_overlapping_areas():
		if i.is_in_group("boss"):
			i.health -= 1
			queue_free()
		
		if !i.is_in_group("player") && (!i.is_in_group("bullet") && !i.is_in_group("boss")) && !i.is_in_group("playerbullet"):
			queue_free()

func _on_VisibilityNotifier2D_screen_exited():
	queue_free()
