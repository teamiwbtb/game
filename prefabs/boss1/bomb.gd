extends Area2D

var bombPrefab = preload("res://prefabs/boss1/bomb2.tscn")

var grav = 40
var vel = Vector2(0,0)


func scatter():
	for i in [-1, 1]:
		var bomb = bombPrefab.instance()
		get_parent().add_child(bomb)
		bomb.global_position = Vector2(global_position.x, 545)
		bomb.vel = 1200
		bomb.dir = i*TAU/24 - TAU/4
	


func _process(delta):
	vel.y += grav*delta
	position.y += vel.y
	
	if global_position.y > 545:
		scatter()
		queue_free()