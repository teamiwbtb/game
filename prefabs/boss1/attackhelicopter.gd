extends Area2D

signal clear

var missilePrefab = preload("res://prefabs/boss1/missile.tscn")
var bulletPrefab = preload("res://prefabs/boss1/realbullet.tscn")
var bombPrefab = preload("res://prefabs/boss1/bomb.tscn")

export(NodePath) var playerPath = ""

var t = 0
var currentSprite = 0

var health = 100

var phaseOrder = [1,2,2,3,5,5,6]
var phase = -1
var finalPhase = 7
var finalPhaseHealth = 25
var spinRate = 2
var maxSpinRate = 10
var spinRateIncrease = 1


func _ready():
	nextPhase()
	$AudioStreamPlayer.play(14.1666)

func nextPhase():
	phase += 1
	if phase >= len(phaseOrder):
		phase -= len(phaseOrder)
	$AnimationPlayer.play("phase"+str(phaseOrder[phase]))

func _process(delta):
	t += delta
	
	while t >= 0.02:
		t -= 0.02
		#currentSprite = (currentSprite + 1) % 2
		$blade.frame = ($blade.frame + 1) % $blade.hframes
		$blade2.frame = ($blade.frame + 1) % $blade2.hframes
	
	$CanvasLayer/TextureProgress.value = health
	
	if health <= 0:
		$AnimationPlayer.playback_speed = 1
		$AnimationPlayer.play("death")
		$CollisionPolygon2D.disabled = true
		emit_signal("clear")
		
	elif health < finalPhaseHealth && phase != finalPhase:
		phase = finalPhase
		$AnimationPlayer.playback_speed = 0
		$AnimationPlayer.play("phase"+str(finalPhase))
		emit_signal("clear")
	
	if phase == finalPhase && health > 0:
		rotation += spinRate*delta
		if spinRate < maxSpinRate:
			spinRate += spinRateIncrease*delta
			if spinRate >= 3:
				$AnimationPlayer.playback_speed += spinRateIncrease*delta/1.5
		if spinRate > maxSpinRate:
			spinRate = maxSpinRate
			$AnimationPlayer.playback_speed = 14/3.0
	
func missile():
	var missile = missilePrefab.instance()
	get_node("../").add_child(missile)
	missile.player = get_node(playerPath)
	missile.global_position = $missilecover.global_position + Vector2(scale.x*10, -4)
	if scale.x == -1:
		missile.rotation = TAU/2
		missile.dir = TAU/2
	missile.dir += rotation
	missile.rotation += rotation
	connect("clear", missile, "clear")


func bullet():
	var bullet = bulletPrefab.instance()
	get_node("../").add_child(bullet)
	bullet.global_position = $bulletspawnpoint.global_position
	var direc = (1/14.0 - randf()/7)+(PI*(scale.x-1)/2.0)
	bullet.vel = Vector2(cos(direc), sin(direc))
	bullet.rotation = rotation
	bullet.speed = 1800
	bullet.scale.x = scale.x
	connect("clear", bullet, "clear")

func spinBullet():
	var bullet = bulletPrefab.instance()
	get_node("../").add_child(bullet)
	bullet.global_position = $bulletspawnpoint.global_position
	var direc = rotation
	bullet.vel = Vector2(cos(direc), sin(direc))
	bullet.rotation = rotation
	bullet.speed = 400
	bullet.scale.x = scale.x
	connect("clear", bullet, "clear")
	
func bomb():
	var bomb = bombPrefab.instance()
	get_parent().add_child(bomb)
	bomb.global_position = global_position
	
func die():
	queue_free()
