extends Area2D

var shrapnel = preload("res://prefabs/boss1/shrapnel.tscn")

var speed = 800
var minSpeed = 450
var dir = 0
var turningSpeed = 7
var player : Object
var health = 1
var canTurn = false


func _process(delta):
	var angleToPlayer = get_angle_to(player.global_position)
	
	if $turnTimer.is_stopped():
		if angleToPlayer < -0.05:
			dir -= turningSpeed * delta
		elif angleToPlayer > 0.05:
			dir += turningSpeed * delta
		else:
			turningSpeed = 0

	if canTurn:
		rotation = dir
	
	position += Vector2(delta * speed * cos(dir),delta * speed * sin(dir))
	
	for i in get_overlapping_areas():
		if i.is_in_group("player"):
			shrapnel("air")
	
	for i in get_overlapping_bodies():
		if i.is_in_group("floor"):
			shrapnel("floor")
			break
		elif i.is_in_group("wall"):
			shrapnel("wall")
			break

	if health <= 0:
		shrapnel("air")
		health = 250
	
func shrapnel(type):
	speed = 0
	$Sprite.hide()
	$Light2D.hide()
	$CollisionShape2D.position = Vector2(1000,1000)
	$Particles2D.emitting = false
	$Particles2D2.emitting = false
	$death.start(2)
	for i in range(10):
		var shard = shrapnel.instance()
		get_node("../").add_child(shard)
		shard.global_position = global_position
		shard.grav = 50
		shard.vel = 900 + randi()%200
		if type == "floor":
			shard.dir = randf()*TAU/9 - TAU/18 - TAU/4- (fmod(rotation+TAU, TAU) + TAU/2)/3 + TAU/4
		elif type == "wall":
			shard.vel -= 300
			if fmod(rotation + TAU, TAU) > TAU/2:
				shard.dir = randf()*TAU/9 - TAU/18 - TAU/4 - (rotation + TAU/4) # Add radomness and angle off wall
			else:
				shard.dir = randf()*TAU/9 - TAU/18 -TAU/4 + (rotation - TAU/4)
		elif type == "air":
			shard.dir = randf()*TAU/18 - 7*TAU/36 + i*TAU/18
			shard.vel -= 200
			
	
	
func _on_death_timeout():
	queue_free()

func _on_turnTimer_timeout():
	canTurn = true

func _on_lock_timeout():
	turningSpeed = 0


func _on_particleTimer_timeout():
	$Particles2D.emitting = true
	$Particles2D2.emitting = true

func clear():
	queue_free()