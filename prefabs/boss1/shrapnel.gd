extends Area2D


export var gravconst = 2700
var grav = 0
var vel = 400
var dir = 0
export var rotationSpeed = 150

var t = 0

func _process(delta):
	position += Vector2(vel * delta * cos(dir), vel * delta * sin(dir))
	grav += gravconst*delta
	position.y += grav*delta
	t += delta
	
	rotation = t*rotationSpeed
	
	if t > 3:
		queue_free()

func _on_VisibilityNotifier2D_screen_exited():
	
	queue_free()
