extends Area2D

var bulletPrefab = preload("res://prefabs/miniboss1/spikebullet.tscn")
export(NodePath) var playerpath = ""
onready var player = get_node(playerpath)
export var debugstart = false
var health = 4

var phaseOrder = [2,1,2,3]
var phase = -1
var dead = false

func _ready():
	$CanvasLayer/healthbar.max_value = health
	if debugstart:
		introphase()
	
func introphase():
	$AnimationPlayer.play("intro")

func nextphase():
	phase += 1
	if phase >= len(phaseOrder):
		phase %= len(phaseOrder)
	$AnimationPlayer.play("phase"+str(phaseOrder[phase]))
	
	
func _process(delta):
	if health <= 0 && !dead:
		die()
		dead = true
	var vec = player.global_position - global_position
	$Sprite.flip_h = vec.x < 0 != (fmod(TAU+rotation+TAU/4, TAU) >= TAU/2)
	$CanvasLayer/healthbar.value = health
		
func shoot(speed, mini, maxi, step):
	var bullets = range(mini, maxi+1, step)
	for i in bullets: 
		var bullet = bulletPrefab.instance()
		get_node("../").add_child(bullet)
		bullet.global_position = global_position
		var vec = player.global_position - global_position
		var direc = atan2(vec.y, vec.x) + (i / 180.0 * PI)
		bullet.vel = Vector2(cos(direc), sin(direc))
		bullet.rotation = direc
		bullet.speed = speed

func shoot2(speed):
	var bullets = [0]

	for i in bullets: 
		var bullet = bulletPrefab.instance()
		get_node("../").add_child(bullet)
		bullet.global_position = global_position
		var vec = player.global_position - global_position
		var direc = atan2(vec.y, vec.x) + (i / 180.0 * PI)
		bullet.vel = Vector2(cos(direc), sin(direc))
		bullet.rotation = direc
		bullet.speed = speed

func die():
	$AnimationPlayer.stop()
	$AnimationPlayer.play("death")
	$AnimationPlayer.connect("animation_finished", self, "queue_free")