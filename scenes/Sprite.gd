extends TextureRect


var p = 0
var t = 0

func _process(delta):
	p -= delta * 200
	t += delta
	margin_left = fmod(p, 1920) - 960
	margin_top = cos(t) * 200 - 540