extends Node2D

var truggered = false
export var triggerRoom = Vector2(0,0)
export(NodePath) var playerPath = ""
onready var player = get_node(playerPath)

func _physics_process(delta):
	if player.room == triggerRoom && !truggered:
		$boss.introphase()
		truggered = true
			