extends Button

var button = self
export var action = "jump"
onready var data = get_node("/root/data")

onready var config = ConfigFile.new()
onready var err = config.load(data.CONFIG_FILE)

func getk():
	set_process_input(true)
	text = "..."

func array_join(arr : Array, glue : String = '') -> String:
    var string : String = ''
    for index in range(0, arr.size()):
        string += str(arr[index])
        if index < arr.size() - 1:
            string += glue
    return string

func _ready():
	connect("pressed", self, "getk")
	set_process_input(false)
	if InputMap.get_action_list(action)[0] is InputEventKey:
		text = config.get_value("input", action, OS.get_scancode_string(InputMap.get_action_list(action)[0].scancode))

func _input(event):
	# Handle the first pressed key
	
	if event is InputEventKey:
		# Register the event as handled and stop polling
		set_process_input(false)
		if not event.is_action("ui_cancel"):
			var scancode = OS.get_scancode_string(event.scancode)
			button.text = scancode
			# Start by removing previously key binding(s)
			for old_event in InputMap.get_action_list(action):
				InputMap.action_erase_event(action, old_event)
			# Add the new key binding
			InputMap.action_add_event(action, event)
			data.save_to_config("input", action, scancode)
