extends KinematicBody2D

export(String,"false","startOnPlayerTouch","onlyOnPlayerTouch") var moveOnPlayerTouch = "false"
export var MAXSPEED = 100

export var looping = false
export var destination = Vector2(0,-20)

onready var defaultPosition = position
var started = false

func _process(delta):
	var speed = 0
	if moveOnPlayerTouch == "onlyOnPlayerTouch":
		for i in $Detector.get_overlapping_bodies():
			if i.is_in_group("player"):
				speed = MAXSPEED
	elif moveOnPlayerTouch == "startOnPlayerTouch" && !started:
		for i in $Detector.get_overlapping_bodies():
			if i.is_in_group("player"):
				speed = MAXSPEED
				started = true
	else:
		speed = MAXSPEED

	var dir : float
	if destination.x != 0:
		dir = atan(destination.y/destination.x)
	else:
		dir = 0
	

	if Input.is_action_just_pressed("respawn"):
		reset()

func reset():
	started = false
	position = defaultPosition
	