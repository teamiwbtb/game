extends Area2D

var played = false
onready var data = get_node("/root/data")
onready var initp = global_position
onready var config = ConfigFile.new()
onready var err = config.load(data.CONFIG_FILE)
func _physics_process(delta):
	for i in get_overlapping_bodies():
		if i.is_in_group("player"):
			data.save_to_config("player", "respawnpos", initp)
			if !played:
				$AnimationPlayer.play("save")
				played = true
	if played && !$AnimationPlayer.is_playing():
		$AnimationPlayer.play("wave")
				
func _ready():
	if config.get_value("player", "respawnpos", Vector2()) == initp:
		$AnimationPlayer.play("wave")