extends Camera2D

var roomSize = Vector2(1024, 600)
onready var player = get_node("../Player")
var t = 0
func _physics_process(delta):
	t += delta
	global_position.x = floor(player.global_position.x / roomSize.x) * roomSize.x
	global_position.y = floor(player.global_position.y / roomSize.y) * roomSize.y