extends Area2D

var speed = 100


var t = 0
var vel = Vector2(0,0)

func _ready():
	set_physics_process(true)

func _physics_process(delta):
	t += delta

	position.x += speed * delta * vel.x
	position.y += speed * delta * vel.y

	if t > 10:
		queue_free()

func clear():
	queue_free()

func _on_VisibilityNotifier2D_screen_exited():
	clear()