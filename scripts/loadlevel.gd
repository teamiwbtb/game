extends Area2D

onready var data = get_node("/root/data")
onready var config = ConfigFile.new()
onready var err = config.load(data.CONFIG_FILE)

export var level = "res://scenes/MainMenu.tscn"
export var location = Vector2()
func _physics_process(delta):
	for i in get_overlapping_bodies():
		if i.is_in_group("player"):
			data.save_to_config("game", "currentscene", level)
			data.save_to_config("player", "respawnpos", location)
			get_tree().change_scene(level)