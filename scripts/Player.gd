extends KinematicBody2D
#a
export var gravity = 981
export var speed = 256
var walkoff_force = 100
var dead = false
var gravmod = 1
var jumps = 0

#var respawnpos = Vector2()
export var maxAirJumps = 2
onready var data = get_node("/root/data")
var v = Vector2()
var facingLeft = false
var walkframe = 0
var jumpForce = 666

onready var config = ConfigFile.new()
onready var err = config.load(data.CONFIG_FILE)

var room = Vector2(0,0)

var bullet_prefab = preload("res://prefabs/playerbullet.tscn")

func _ready():
	var rp = config.get_value("player", "respawnpos", Vector2(0, 0))
	if rp != Vector2(0, 0):
		global_position = rp
func _physics_process(delta):
	var roomSize = Vector2(1024, 600)
	room = Vector2(floor(global_position.x / roomSize.x),floor(global_position.y / roomSize.y))

	var move = 0
	if !is_on_floor():
		if v.y > 0:
			v.y += delta * gravity * gravmod
		else:
			v.y += delta * gravity * gravmod
	else:	
		jumps = 0
		v.y = walkoff_force

	move += Input.get_action_strength("rightAnalogue") + Input.get_action_strength("right")
	move -= Input.get_action_strength("leftAnalogue") + Input.get_action_strength("left")
	
	move = min(move, 1)
	move = max(move, -1)
	
	if Input.is_action_pressed("right") && !Input.is_action_pressed("moonwalk"):
		facingLeft = false
	elif Input.is_action_pressed("left") && !Input.is_action_pressed("moonwalk"):
		facingLeft = true
	
	$Sprite.flip_h = facingLeft
	
	if move != 0 && is_on_floor():
		$Sprite.frame = int(walkframe / 5) + 1
	else:
		$Sprite.frame = 0
		
	walkframe += abs(move)
	walkframe = fmod(walkframe, 10)
	
	if is_on_ceiling():
		v.y = max(v.y, 0)
		gravmod = 1.5
	if  jumps < maxAirJumps && Input.is_action_just_pressed("jump"):
		v.y = -jumpForce
		gravmod = 2
		jumps += 1

	if !Input.is_action_pressed("jump") && v.y < 0:
		gravmod = 6
	elif !Input.is_action_pressed("jump") || v.y >= 0:
		gravmod = 1.5
	

	if !dead:
		move = move_and_slide(Vector2(v.x + move * speed, v.y), Vector2(0, -1))
		
		for i in $deathcheck.get_overlapping_areas():
			if i.is_in_group("kill"):
				kill()
		for i in $deathcheck.get_overlapping_bodies():
			if i.is_in_group("kill"):
				kill()
		for i in $bulletcheck.get_overlapping_areas():
			if i.is_in_group("bullet"):
				kill()
	
	if Input.is_action_just_pressed("shoot"):
		$bullettimer.stop()
	
	if Input.is_action_pressed("shoot") && !dead:
		if $bullettimer.time_left == 0:
			shoot()
			$bullettimer.start()
	
	if Input.is_action_just_pressed("respawn"):
		get_tree().reload_current_scene()

func shoot():
	var bullet = bullet_prefab.instance()
	get_node("../").add_child(bullet)
	bullet.global_position = global_position
	if facingLeft:
		bullet.speed *= -1
		bullet.scale.x = -1

func kill():
	$blood.emitting = true
	$bloodtimer.start()
	$Sprite.hide()
	dead = true
	$CollisionShape2D.disabled = true
	$deathlabel.show()

func _on_bloodtimer_timeout():
	$blood.emitting = false
