extends Control

const level1 = "res://scenes/jungle.tscn"

onready var data = get_node("/root/data")
onready var config = ConfigFile.new()
onready var err = config.load(data.CONFIG_FILE)

func start():
	var next = config.get_value("game", "currentscene", level1)
	if next != "res://scenes/MainMenu.tscn":
		get_tree().change_scene(next)
	else:
		get_tree().change_scene(level1)
func _ready():
	var next = config.get_value("game", "currentscene", level1)
	if next != level1:
		$Buttons/Play.text = "Continue"
	$Buttons/Settings.connect("pressed", $SettingsDialog, "popup_centered")
	$Buttons/Play.connect("pressed", self, "start")
	$Buttons/Quit.connect("pressed", get_tree(), "quit")